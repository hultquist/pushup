#include <Eigen/Core>
#include "eval-part.h"
#include <set>

using namespace std;

unsigned int getPartitionHedgeCut(set<set<int>> hedges, int *partitions)
{
    unsigned int hedge_cut = 0;
    for(auto hh = hedges.begin(); hh != hedges.end(); ++hh) {
        set<int> hedge_parts;
        for(auto nn = hh->begin(); nn != hh->end(); ++nn) {
            hedge_parts.insert(partitions[*nn-1]);
        }
        hedge_cut += hedge_parts.size() - 1;
    }
    return hedge_cut;
}
