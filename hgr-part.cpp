#include <Eigen/Core>
#include <Spectra/SymEigsSolver.h>
// <Spectra/MatOp/DenseSymMatProd.h> is implicitly included
#include <Spectra/MatOp/SparseSymMatProd.h>
#include <iostream>
#include <tuple>
#include <set>
#include <fstream>
#include <numeric>
#include <climits>
#include <chrono>

#include "parse-hgr.h"
#include "eval-part.h"
#include "asa136.hpp"

using namespace Spectra;
using namespace std;
using namespace Eigen;

int main(int argc, char *argv[])
{
    int k = 2;
    cout << k << " partitions" << endl;
    MatrixXd A;
    set<set<int>> hedges;

    auto parseHGR_start = chrono::steady_clock::now();
    tie(A, hedges) = parseHGR(argv[1]);
    auto parseHGR_end = chrono::steady_clock::now();
    auto parseHGR_msecs = std::chrono::duration_cast<std::chrono::milliseconds>(parseHGR_end - parseHGR_start).count();
    cout << "Parsing hypergraph took: " << parseHGR_msecs << " milliseconds" << endl;

    auto eigensolve_start = chrono::steady_clock::now();
    MatrixXd D = (A*VectorXd::Ones(A.cols())).asDiagonal();
    MatrixXd L = D - A;
    SparseMatrix<double> Lsparse = L.sparseView();
    // cout << A << "\n";
    //cout << A.diagonal() << "\n";
    //cout << D << endl;
    // We are going to calculate the eigenvalues of M

    // Construct matrix operation object using the wrapper class DenseSymMatProd
    SparseSymMatProd<double> op(Lsparse);

    // Construct eigen solver object, requesting the k smallest eigenvalues
    int ncv = 6;
    if(ncv > A.cols()) {
        ncv = A.cols();
    }
    SymEigsSolver<double, SMALLEST_ALGE, SparseSymMatProd<double> > eigs(&op, k, ncv);

    // Initialize and compute
    eigs.init();
    int nconv = eigs.compute(10000000, 1e-6);
    if(nconv < k) {
        cout << "ERROR: Did not converge! Try tweaking solver parameters." << endl;
	exit(1);
    }

    // Retrieve results
    VectorXd evalues;
    Matrix<double, Dynamic, Dynamic, RowMajor> evectors;
    if(eigs.info() == SUCCESSFUL) {
        evalues = eigs.eigenvalues();
        evectors = eigs.eigenvectors().leftCols(k-1);
    }

    auto eigensolve_end = chrono::steady_clock::now();
    auto eigensolve_msecs = std::chrono::duration_cast<std::chrono::milliseconds>(eigensolve_end - eigensolve_start).count();
    cout << "Finding eigenvalue took: " << eigensolve_msecs << " milliseconds" << endl;

    auto cluster_start = chrono::steady_clock::now();

    //cout << "evectors has " << evectors.rows() << " rows and " << evectors.cols() << " cols" << endl;
    int ndim = k-1;
    // double *pts = evectors.data();
    // double *centers = new double[k*ndim];
    // int *partitions = new int[evectors.rows()];
    // int *pts_per = new int[k];
    // double *wss = new double[k];
    // int IFAULT;
    // memset(centers, 0, sizeof(centers));
    // memset(partitions, 0, sizeof(partitions));
    // memset(pts_per, 0, sizeof(pts_per));
    // memset(wss, 0, sizeof(wss));
    // for(int c=0; c<k; c++) {
    //     for(int dim=0; dim<ndim; dim++) {
    //         centers[c*ndim + dim] = evectors(c,dim);
    //     }
    // }

    // kmns(pts, evectors.rows(), ndim, centers, k, partitions, pts_per, 50, wss, &IFAULT);
    //cout << "kmns code: " << IFAULT << endl;
    //cout << evectors << endl;
    vector<int> nodes(evectors.rows());
    iota(begin(nodes), end(nodes), 0); // Fill with 0, 1 ... N
    sort(nodes.begin(), nodes.end(), [&evectors]( const int& lhs, const int& rhs )
        {
            return evectors(lhs, 0) < evectors(rhs, 0);
        });
    int _45cutoff = (int)ceil(0.45 * (float)(nodes.size())) -1;
    int _55cutoff = (int)floor(0.55 * (float)(nodes.size())) -1;
    int *partitions = new int[evectors.rows()];
    for(int i=0; i<evectors.rows(); i++) {
        if (i <= _45cutoff) {
            partitions[nodes[i]] = 0;
        } else {
            partitions[nodes[i]] = 1;
        }
    }
    vector<int> cuts(_55cutoff - _45cutoff + 1);
    unsigned int mincut = UINT_MAX;
    int mincut_loc = -1;
    for(int i = _45cutoff; i <= _55cutoff; i++) {
        partitions[nodes[i]] = 0; // Move one node over at a time and eval cut
        int cut = getPartitionHedgeCut(hedges, partitions);
        if(cut < mincut) {
            mincut_loc = i;
            mincut = cut;
        }
        //cout << "i: " << i << " cut: " << cut << endl;
    }
    //cout << "Best cut at elem " << mincut_loc << endl;
    // Construct best partition
    for(int i=0; i<evectors.rows(); i++) {
        if (i <= mincut_loc) {
            partitions[nodes[i]] = 0;
        } else {
            partitions[nodes[i]] = 1;
        }
    }

    auto cluster_end = chrono::steady_clock::now();
    auto cluster_msecs = std::chrono::duration_cast<std::chrono::milliseconds>(cluster_end - cluster_start).count();
    cout << "Clustering took: " << cluster_msecs << " milliseconds" << endl;

    cout << "Final partition hyperedge cut: " << getPartitionHedgeCut(hedges, partitions) << endl;
    cout << "Balance: " << mincut_loc+1 << " vs " << (evectors.rows() - (mincut_loc+1)) << endl;

    string partfile_path(argv[1]);
    partfile_path += ".part." + std::to_string(k) + ".ours";
#ifdef SIMPLEWEIGHTS
    partfile_path += ".simpleweights";
#endif
    ofstream partfile;
    partfile.open(partfile_path);
    for(int i=0; i<evectors.rows(); i++) {
        partfile << partitions[i] << endl;
    }
    partfile.close();

    auto total_msecs = cluster_msecs + eigensolve_msecs + parseHGR_msecs;
    cout << "Total time elapsed: " << total_msecs << " milliseconds" << endl;

    return 0;
}

