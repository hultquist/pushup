#include <Eigen/Core>
#include <iostream>
#include <set>
#include <tuple>


std::tuple<Eigen::MatrixXd, std::set<std::set<int>>> parseHGR(char *hgr_file_path);
