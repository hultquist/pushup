import pandas
import sys
from matplotlib import pyplot as plt
import numpy as np

fig, axes = plt.subplots(2,2)

axes = [axes[0,0], axes[0,1], axes[1,0], axes[1,1]]
metrics = ["Hyperedge Cut", "External Degrees", "Scaled Cost", "Absorption"]
min_max = ["Minimize","Minimize","Minimize","Maximize"]

for inp, axis, metric, max_or_min in zip(sys.argv[1:5], axes, metrics, min_max):
    data = pandas.read_csv(inp, nrows=12)

    bxp_stats= []
    rand_medians = []
    rand_errs = np.zeros((2,data.shape[0]))
    ours_pts = []
    hmetis_pts = []
    benchmarks = []
    for index, row in data.iterrows():
        bxp_stats.append({
            "med": row["Rand median"],
            "q1": row["Rand Q1"],
            "q3": row["Rand Q3"],
            "whislo": row["Rand min"],
            "whishi": row["Rand max"],
            "label": row["Input"]
        })
        rand_medians.append(row["Rand median"])
        rand_errs[0,index] = row["Rand median"] - row["Rand min"]
        rand_errs[1,index] = row["Rand max"] - row["Rand median"]
        ours_pts.append(row["Ours"])
        hmetis_pts.append(row["hMETIS"])
        benchmarks.append(row["Input"].replace("ISPD98_",""))

    indices = np.arange(data.shape[0])
    # axes.bxp(bxp_stats, showfliers=False)
    width = 0.25
    # axes.scatter(range(1,len(rand_medians)+1), rand_medians, s=4, c='gray', label="Random hypergraph")
    axis.bar(np.add(indices,width), rand_medians, yerr=rand_errs, width=width, label="Random")
    axis.bar(indices, ours_pts, width=width, color="limegreen", label="Ours")
    axis.bar(np.add(indices,-width), hmetis_pts, width=width, color="Red", label="hMETIS")
    axis.set_xlabel("Benchmark")
    axis.set_ylabel(metric)
    axis.set_xticks(indices)
    axis.set_xticklabels(benchmarks, rotation=65)
    axis.set_title("{} ({})".format(metric, max_or_min))
    axis.ticklabel_format(axis='y', style='sci', scilimits=(0,0))

# fig.suptitle("Ours vs. hMETIS vs. 100 Random Partitions")
plt.margins(left=0.12, bottom=0.28, right=0.99, top=0.93)
plt.legend()
plt.tight_layout()
plt.show()
