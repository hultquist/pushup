import sys
from collections import Counter
from itertools import combinations

hgr = sys.argv[1]

hedges = set()
num_verts = 0
with open(hgr, 'r') as hgrf:
    nhedges, nverts = hgrf.readline().split()
    for line in hgrf:
        verts = [int(x) for x in line.split()]
        for v in verts:
            if v > num_verts:
                num_verts = v
        hedges.add(frozenset(verts))

edges = Counter()
for hedge in hedges:
    for edge in combinations(list(hedge), 2):
        edges[frozenset(edge)] += 1

print("%d %d 1" % (len(edges), num_verts))
for (v1, v2), weight in edges.items():
    print("%d %d %d" % (weight, v1, v2))

