# This python program evaluates our hgr partitioner against
# random graph partitions in terms of hyperedge cut.
# It uses hmetis to measure th hyperedge cut

import subprocess
import random
import re
import sys
import tempfile
from matplotlib import pyplot as plt
from gen_hgr import gen_hgr
import numpy as np
import multiprocessing

hedge_cut_re = re.compile(r"Hyperedge Cut:\s*(\d+)")
ext_deg_re = re.compile(r"Sum of External Degrees:\s*(\d+)")
cost_re = re.compile(r"Scaled Cost:\s*(\d+(.\d+e-?\d+)?)")
absorb_re = re.compile(r"Absorption:\s*(\d(.\d+)?)")

parseHGR_re = re.compile(r"Parsing hypergraph took: (\d+) milliseconds")
eigensolve_re = re.compile(r"Finding eigenvalue took: (\d+) milliseconds")
cluster_re = re.compile(r"Clustering took: (\d+) milliseconds")

def run_hmetis(inputf, partitionf=None):
    if partitionf is None:
        partitionf = ""
    hmetis = subprocess.Popen(['./hmetis/run_hmetis.sh', inputf, partitionf],
                              stdout=subprocess.PIPE)
    output, _err = hmetis.communicate()
    # print(output)
    hedge_cut_match = hedge_cut_re.search(output)
    ext_deg_match = ext_deg_re.search(output)
    cost_match = cost_re.search(output)
    absorb_match = absorb_re.search(output)
    hedge_cut = int(hedge_cut_match.groups()[0])
    ext_deg = int(ext_deg_match.groups()[0])
    cost = float(cost_match.groups()[0])
    absorb = float(absorb_match.groups()[0])
    return hedge_cut, ext_deg, cost, absorb


def run_hgr_part(inputf, simpleweights=False):
    if simpleweights:
        exe = './hgr-part-simpleweights'
    else:
        exe = './hgr-part'
    hmetis = subprocess.Popen([exe, inputf],
                              stdout=subprocess.PIPE)
    output, _err = hmetis.communicate()

    # Capture time duration of each step
    parseHGR_match = parseHGR_re.search(output)
    eigensolve_match = eigensolve_re.search(output)
    cluster_match = cluster_re.search(output)
    parseHGR = int(parseHGR_match.groups()[0])
    eigensolve = int(eigensolve_match.groups()[0])
    cluster = int(cluster_match.groups()[0])
    return parseHGR, eigensolve, cluster

def eval_rand_part(inputf, n_nodes):
    while True:
        partition = []
        for _ in range(0, n_nodes):
            partition.append(random.choice([0,1]))
        if (sum(partition) <= 0.55*n_nodes) and (sum(partition) >= 0.45*n_nodes):
            break
    # print(partition)
    partitionf = tempfile.NamedTemporaryFile()
    for p in partition:
        partitionf.write(str(p)+"\n")
    partitionf.flush()
    # print(partitionf.name)
    perf_data = run_hmetis(inputf, partitionf.name)
    partitionf.close()
    return perf_data

def do_histogram(axis, rands, ours, hms, title):
    axis.hist(rands)
    axis.axvline(ours, color="green", label="HGR-PART")
    axis.axvline(hms, color="red", label="hMETIS")
    axis.set_xlabel(title)
    minx = min(rands+[ours, hms])
    maxx = max(rands+[ours, hms])
    axis.set_xlim(minx-(maxx-minx)*.1, maxx+(maxx-minx)*.1)

def do_csv(rands, ours, hms, title, inputf):
    print "{},{},{},{},{},{},{},{},".format(inputf,
                                            hms,
                                            ours,
                                            min(rands),
                                            np.quantile(rands, 0.25),
                                            np.median(rands),
                                            np.quantile(rands,
                                            0.75),
                                            max(rands)),

def eval_parts():
    inputf = sys.argv[1]
    partf = sys.argv[2]
    with open(inputf) as ff:
        n_nodes = int(ff.readline().split()[1])
    hec_list = []
    ed_list = []
    c_list = []
    a_list = []
    num_rand = 100
    for _ in range(0, num_rand):
        hec, ed, c, a = eval_rand_part(inputf, n_nodes)
        hec_list.append(hec)
        ed_list.append(ed)
        c_list.append(c)
        a_list.append(a)
    # run_hgr_part(inputf)
    our_hec, our_ed, our_c, our_a = run_hmetis(inputf, partf)
    hm_hec, hm_ed, hm_c, hm_a = run_hmetis(inputf)

    print ""
    do_csv(hec_list, our_hec, hm_hec, "Hyperedge Cut (minimize)", inputf)
    do_csv(ed_list, our_ed, hm_ed, "External Degrees (minimize)", inputf)
    do_csv(c_list, our_c, hm_c, "Scaled Cost (minimize)", inputf)
    do_csv(a_list, our_a, hm_a, "Absorption (maximize)", inputf)

    # Do plot
    # fig, axes = plt.subplots(2, 2)
    # do_histogram(axes[0,0], hec_list, our_hec, hm_hec, "Hyperedge Cut (minimize)")
    # do_histogram(axes[0,1], ed_list, our_ed, hm_ed, "External Degrees (minimize)")
    # do_histogram(axes[1,0], c_list, our_c, hm_c, "Scaled Cost (minimize)")
    # do_histogram(axes[1,1], a_list, our_a, hm_a, "Absorption (maximize)")
    # fig.suptitle("%s: %d random partitions vs. ours vs. hMETIS" %
    #               (inputf, num_rand))
    # plt.legend()
    # plt.show()

def _profile_runtime_once(args):
    num_verts, num_edges, max_degree = args
    rand_hgr = tempfile.NamedTemporaryFile()
    gen_hgr(rand_hgr, num_verts, num_edges, max_degree)
    rand_hgr.flush()
    parseTime, eigenTime, clusterTime = run_hgr_part(rand_hgr.name)
    row = "{},{},{}".format(parseTime, eigenTime, clusterTime)
    rand_hgr.close()
    return row

def profile_runtime():
    print "Parse (msec),Eigen Solver (msec),Clustering (msec)"
    num_verts, num_edges, max_degree = [int(arg) for arg in sys.argv[1:4]]
    pool = multiprocessing.Pool(multiprocessing.cpu_count())
    rows = pool.map(_profile_runtime_once, [(num_verts, num_edges, max_degree)]*100)
    for r in rows:
        print r

def _compare_weighting_once(args):
    num_verts, num_edges, max_degree = args
    rand_hgr = tempfile.NamedTemporaryFile()
    gen_hgr(rand_hgr, num_verts, num_edges, max_degree)
    rand_hgr.flush()
    run_hgr_part(rand_hgr.name, simpleweights=False)
    partf = rand_hgr.name + ".part.2.ours"
    our_hec, our_ed, our_c, our_a = run_hmetis(rand_hgr.name, partf)
    results = "Probabilistic,{},{},{},{}\n".format(our_hec, our_ed, our_c, our_a)
    run_hgr_part(rand_hgr.name, simpleweights=True)
    simple_partf = rand_hgr.name + ".part.2.ours.simpleweights"
    simple_hec, simple_ed, simple_c, simple_a = run_hmetis(rand_hgr.name, simple_partf)
    results += "Simple,{},{},{},{}".format(simple_hec, simple_ed, simple_c, simple_a)
    return results

def compare_weighting_schemes():
    """
    Compares our weighting scheme to a more simplistic one where
    all edges of the 2-section graph are incremented by one
    for every hyperedge they're a part of.
    """
    print "Weight Scheme,Hyperedge Cut,External Degrees,Scaled Cost,Absorption"
    num_verts, num_edges, max_degree = [int(arg) for arg in sys.argv[1:4]]
    pool = multiprocessing.Pool(multiprocessing.cpu_count())
    rows = pool.map(_compare_weighting_once, [(num_verts, num_edges, max_degree)]*100)
    for r in rows:
        print r

if __name__ == "__main__":
    # profile_runtime()
    # eval_parts()
    compare_weighting_schemes()
