\section{Experimental Methodology}
\label{sec:expmethod}

\subsection{Partition Quality}

As stated previously, our goal is to find an
optimal partitioning of the hypergraph --- optimal in terms of its:
\begin{itemize}
    \item Hyperedge cut --- the number of hyperedges that span 2 or more partitions.
    \item External degrees --- sum of the number of partitions spanned by every hyperedge
    \item Scaled cost --- defined as \\
    $$\frac{1}{n(k - 1)}\sum_{i=1}^{k}\frac{ |E(P_i)| }{ |P_i| }$$\\
    where $P_i$ is a partition. Note the above is valid only for hypergraphs with unweighted nodes.
    \item Absorption --- defined as \\
    $$\sum_{i=1}^{k}\sum_{e \in E|e \cap P_i \ne 0}\frac{ |e \cap P_i| - 1}{ |e| - 1}$$\\
    where $E$ is the set of hyperedges, $|e \cap P_i|$ is the number of nodes of hyperedge $e$ that are also in partition $P_i$, and $|e|$ is the number of nodes in hyperedge $e$.
\end{itemize}
An optimal partition will maximize absorption and minimize hyperedge cut, external degrees and scaled cost.

All that said, it's difficult to know what the optimal partition is for an arbitrary hypergraph.
All existing solvers are heuristic-based,
meaning they are not guaranteed to arrive at the optimal solution.
For this reason, we compare \sysname{}'s partitions to the state-of-the-art and to naive solutions,
rather than the optimal solution.
The state-of-the-art that we compare against is hMETIS~\cite{karypis_multilevel_1999}.
The naive solution that we compare against is one which randomly generates partitions.
We hope to see that \sysname{}'s partition quality is much better than that of
the random partitions, and that we come close to or possibly exceed the quality
achieved by hMETIS.

We evaluate \sysname{} on a hypergraph benchmark suite: ISPD98~\cite{ispd98}.
This benchmark suite consists of a number of hypergraphs varying in size,
each of which represents an electrical schematic netlist.
We generate partitions for each benchmark input using \sysname{}, hMETIS, and our random generator.
hMETIS is run with the following parameters:
\begin{itemize}
    \item \textbf{Nparts = 2} for 2 partitions
    \item \textbf{UBfactor = 5} to achieve worst-case balance of 45\%-55\%
    \item \textbf{Nruns = 10}, the number of iterations
    \item \textbf{CType = 4} for hyperedge grouping
    \item \textbf{RType = 1} for Fiduccia-Mattheyses (FM) refinement scheme
    \item \textbf{VCycle = 3} to perform V -cycle refinement on each intermediate solution.
    \item \textbf{Reconst = 1} to reconstruct cut hyperedges in each partition
\end{itemize}

We then use hMETIS itself to calculate the quality of each of these partitions, since hMETIS
supports evaluating an existing partition. For every benchmark we generate 100 random partitions
so that we can get a sense of the distribution in quality.

\subsection{Evaluating Probabilistic Edge Weighting}
\label{sec:expmethod_weight}

The technique for calculating edge weights of the 2-section graph, presented in Section~\ref{sec:2sec},
is fairly novel. We want to evaluate whether this helps or hurts our partition quality compared
to a simplistic tecnique, and to what degree.

The simplistic edge weighting technique we will compare against is one where the edge weights in
the 2-section graph are simply incremented by one for every clique they are a part of.
More formally:
\begin{align*}
    e \in E\\
    u, v \in e\\
    w(e) = \big\lvert\{h~\vert~u,v \in h,~h \in H\}\big\rvert
\end{align*}
where $E$ is the set of edges of the 2-section graph, $u$ and $v$ are nodes, $w(e)$ is the weight of an edge $e$, $H$ is the set of hyperedges of the hypergraph, and $\big\lvert\{h~\vert~u,v \in h,~h \in H\}\big\rvert$ is the number of hyperedges containing both $u$ and $v$.

To compare these techniques, we will run \sysname{} on randomly-generated
hypergraphs with and without probabilistic edge weighting.
These random hypergraphs will vary in size; we will test 100 different
random hypergraphs of each size. These sizes are:
\begin{itemize}
    \item 1000 nodes, 1000 hyperedges, rank 20
    \item 1100 nodes, 1100 hyperedges, rank 22
    \item 1200 nodes, 1200 hyperedges, rank 24
    \item 1300 nodes, 1300 hyperedges, rank 26
    \item 1400 nodes, 1400 hyperedges, rank 28
    \item 1500 nodes, 1500 hyperedges, rank 30
\end{itemize}
We hope to see that partition quality improves with probabilistic weighting.

\subsection{Runtime}

Another goal of \sysname{} is to arrive at a high quality partition in a reasonable amount
of time. For that reason, we will measure the runtime of \sysname{} for hypergraphs of different
sizes. We will randomly generate hypergraphs of different sizes and record the amount
of time \sysname{} spends in each step of the algorithm, these being:
\begin{enumerate}
    \item Parsing the hypergraph file
    \item Solving for the eigenpairs
    \item Clustering the nodes
\end{enumerate}
We again test using 100 different random hypergraphs of
the sizes listed in Section~\ref{sec:expmethod_weight}.
