\section{Algorithm}
\label{sec:algorithm}

Given a set of nodes $N$ and the hyperedges that connect them, we would like to find a partition of these nodes.

First, we will explain the Spectral Partitioning Algorithm as it applies to standard graphs (not hypergraphs). Then, we will show how we can transform a hypergraph into a representative standard graph.

\subsection{Spectral Partitioning}

This section contains a quick summary of spectral partitioning as described in~\cite{original_spectral}.

Starting with the simplified problem of two partitions, our ultimate goal is to minimize this cost function:
\begin{align*}
\rho(S) = \min_S \frac{vol(\delta(S))}{|S| |\bar{S}|}
\end{align*}
where $S$ and $\bar{S}$ represent the two partitions and $vol(\delta(S)) = vol(\delta(\bar{S}))$ is the magnitude of the cut across these partitions. Notice that in addition to minimizing the cut, we are also using the denominator to penalize drastically different sized partitions. However, we will address partition sizes more directly in the clustering portion of the algorithm.

Let $D$ be a diagonal matrix such that $D_{ii}$ represents the total weights leaving node $i$ and let $A$ be the adjacency matrix. Then, Laplacian matrix $L$ can easily be found as $L = D - A$. Also, let $\mathbf{z}$ be a vector of size $|N|$ such that $\mathbf{z}_i = 1$ if node $i$ is in Partition A and $\mathbf{z}_i = 0$ if node $i$ is in Partition B. Thus, $\mathbf{z}$ is essentially an indicator vector that fully describes the partition.

Another equivalent way to represent $vol(\delta(S))$ is by $\mathbf{z}^T L \textbf{z}$, which can be shown by simply expanding this multiplication. However, minimizing $\mathbf{z}^T L \textbf{z}$ is a difficult NP-Hard problem, since each element of $\mathbf{z}$ is restricted to be one of two possible discrete values. Therefore, we aim to simply approximate with a continuous vector $\mathbf{u}$ and later use clustering techniques to construct $\mathbf{z}$ from $\mathbf{u}$. There is no mathematical guarantee that an optimal $\mathbf{u}$ will yield an optimal $\mathbf{z}$ but it seems to work well in practice.

After some manipulation, and by following the Courant-Fisher theorem~\cite{horn_matrix_2012}, the solution can be found by solving an
eigenvalue problem involving $\mathbf{u}$:
\begin{align*}
L \mathbf{u} = \lambda \mathbf{u}
\end{align*}

This equation will have multiple eigenvalues/vectors as solutions. Each eigenpair represents a partition $\mathbf{u}$ and the corresponding size of a cut, $\lambda$. If we order the eigenpairs from smallest to largest as below:
\begin{align*}
(\lambda_1, \mathbf{u}_1), \dots (\lambda_N, \mathbf{u}_N)
\end{align*}
where $(\lambda_1, \mathbf{u}_1)$ contains the smallest eigenvalue, then we are interested in pairs 2 through $p$, where $p$ is the desired number of partitions. In doing so, we leave out the smallest eigenpair because the eigenvalue is 0, which corresponds to a trivial partition where every node is in the same partition. We are therefore left with $p-1$ eigenvectors to use for clustering. In this project, we focus on finding two partitions, so we solve for only the second smallest eigenpair.

\subsection{Hypergraph 2-section}
\label{sec:2sec}

In the case of hypergraphs, this eigenvalue problem becomes much more difficult. The adjacency matrix must be generalized to a set of adjacency tensors of different orders. For a hypergraph with a max hyperedge size of $N$, you need adjacency tensors of order $2$ through $N$. Each adjacency tensor of order $M$ can only describe hyperedges with $M$ nodes. This makes the spectral algorithm as-is difficult to apply to hypergraphs. We would need to solve the above minimization problem multiple times over for high-order tensors, and there is minimal programming language/library support for tensor eigenvalue problems. The Tensor Algebra Compiler can perform tensor algebra, but it cannot solve tensor eigenvalue problems~\cite{kjolstad_taco:_2017}.

We have elected to simplify the problem by reducing hypergraphs to weighted graphs. We do this by redrawing each hyperedge as a clique. That is, we fully connect the set of nodes in the hyperedge. We combine these cliques to form a 2-section graph representation of the hypergraph. If a clique contains an edge that is already in our 2-section graph, then the weight of the edge in the clique is added to the weight of the edge in the 2-section graph.

A problem arises when you consider how this affects the cost function. Recall that the cost function aims to reduce the edge cut. By transforming the hyperedge into a clique of multiple edges, we have artificially increased the cost of cutting this hyperedge, since each edge of the clique will count toward the cut separately. This will unneccesarily discourage cuts of high-degree hyperedges. To counteract this effect, we will scale the weights of the clique's edges.

Ideally, the cut of the clique would always equal the hyperedge cut, i.e. 1. Unfortunately, we can't know ahead of time exactly how the clique will be cut so we cannot guarantee this will be true. Therefore, we elected to take a probabilistic approach to determining the weights of edges in the newly generated clique. We make the following assumptions: first, we give each node an equal probability of being in either partition; second, we assume that the placement of each node in its partition is independent. In reality, the second assumption is likely not always accurate, but for our purposes was a decent approximation. With these assumptions, the number of nodes in each partition can be modeled as a binomial distribution. Then we can find the expected value of the edge cut when a clique is partitioned (or in the original hypergraph, when a hyperedge is cut). This technique is what lends \sysname{} the leading P in its name, which stands for ``Probabilistic''.

Let $N$ be the number of nodes in the clique, and let $I \leq N$ be the number of nodes in the first partition. $I$ is simply a binomial distribution with $p = 0.5$.

If there are $i$ nodes in partition $A$, the number of edges cut across this partition is $C(i) = i*(N-i)$ since each node in $A$ connects to each node outside of $A$. One more thing to note is that we want the expected edge cut \textbf{given that an edge was cut}. This is because if the clique was not cut, we do not care what the weights were because they do not contribute to the hyperedge cut.

\begin{align*}
E(C(I) | \text{cut}) & = \sum_{i=0}^{N} C(i) P(I = i  | \text{cut}) \\
P(I = i  | \text{cut}) & = \frac{P(\text{cut} | I = i) P(I = i)}{P(\text{cut})}
= \frac{I_{0 < i < N} \binom{N}{i} \big( \frac{1}{2} \big)^N}{1 - (\frac{1}{2})^{N-1}} \\
E(C(I) | \text{cut}) & = \sum_{i=0}^{N} i*(N-i) \frac{I_{0 < i < N} \binom{N}{i} \big( \frac{1}{2} \big)^N}{1 - (\frac{1}{2})^{N-1}}
\end{align*}
And then, after some algebraic simplification:
\begin{align*}
E(C(I) | \text{cut}) & = \sum_{i=1}^{N-1} i * (N-i) \frac{\binom{N}{i}}{2^{N} - 2}
\end{align*}

This result allows us to scale the weights of all edges in the clique so that we can hope to achieve an edge cut of 1, approximating the behavior of the hyperedge. To do this, we assign a weight of $\frac{1}{E(C(I|\text{cut}))}$ to each edge in the 2-section. Therefore, if our assumptions hold, our 2-section will be a reasonable approximation to the original graph.

This process will be performed for every hyperedge in the hypergraph. If the clique for a hyperedge contains an edge that is already in the 2-section graph from another hyperedge, the calculated weights for the edge will be summed together.

\begin{figure}[]
    \centering
    \includegraphics[width=\columnwidth]{img/edgeweight.png}
    \caption{Example edge weights for a hyperedge consisting of 4 nodes.}
    \label{fig:edgeweight}
\end{figure}

Figure~\ref{fig:edgeweight} shows the weights calculated for the 2-section clique edges corresponding to a 4-node hyperedge. The expected value of the cut of this clique is now 1.

We expect the partition for the weighted 2-section graph to be of good quality for the original hypergraph.

\subsection{Equal Sized Clustering}

An important criteria when partitioning graphs and hypergraphs is that the partitions need to be relatively balanced. Usually the level of balance is provided as a parameter to the solver. In our case, we aim for
a worst-case imbalance of 45\% to 55\%. In other words, the smallest partition may contain as few as 45\% of the nodes in the hypergraph.

Our first approach was to use the K-means clustering algorithm. However, one problem with K-means is that it does not guarantee balanced clusters. To remedy this, one could do post-processing
on the clusters to restore balance. If it so happens that K-means produces unbalanced clusters, one might take the larger cluster and proceed to force out points until it is balanced. To do this, one would find the node in the larger cluster that is closest to the centroid of the smaller cluster and force that node into the smaller cluster. Then, the centroids of the two clusters (but not the clusters themselves) are recomputed and the process repeats until the clusters are of approximately equal size.

However, this process can run into problems when trying to divide more than 2 partitions. At first, we did not consider this problematic since we are focusing on the 2 partition case. However, since only $p-1$ eigenvectors are used for clustering, we realized that K-means was simply unnecessary to cluster this one-dimensional space. Therefore, we decided to abandon this technique and develop a simpler brute force approach.

The lone eigenvector used to find our two partitions effectively linearly orders the hypergraph's nodes. Due to the size constraints of the partitions, we decided not to use k-means clustering, which doesn't guarantee balanced partitions. Instead, we exploited this ordered list to directly find the optimal, balanced partition.

Because the most unbalanced partition allowed is a 45\%-55\% split, we finalized the top and bottom 45 percent of the ordered nodes into their respective partitions. Then, we evaluated the hyperedge cut on every boundary within this permissible range. For a hypergraph with $N$ nodes and $E$ hyperedges, this corresponds to simply evaluating the cut of $0.1N$ possible partitions, each of which takes $O(E)$ time. It would be possible to speed up the evaluation since there is a lot of redundant computation when it comes to the fixed 90 percent of edges. However, we found this to run in a reasonable time and not be a bottleneck in our calculations so we did not perform this optimization.

