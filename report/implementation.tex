\section{Implementation}
\label{sec:implementation}

The hypergraph partitioning algorithm presented in
Section~\ref{sec:algorithm} was implemented in C++
with the help of some third-party libraries: notably Eigen for
working with matrices~\cite{eigen} and
Spectra for solving for the eigenpairs~\cite{spectra}.
The program accepts inputs in the format used by hMETIS,
the state-of-the-art hypergraph partitioning
program~\cite{karypis_multilevel_1999}.

The first step is to build the adjacency matrix for the
weighted 2-section graph associated with this hypergraph.
The adjacency matrix is initialized as a $N \times N$ matrix
of zeroes, where $N$ is the number of nodes
in the hypergraph. The program iterates over the
hyperedges of the hypergraph, and for each pair
of vertices in that hyperedge calculates
the weight of their edge. The weight is based on the
expected value of a random cut of this hyperedge, as
described in Section~\ref{sec:2sec}. The calculated
edge weight is added to the two cells in the adjacency matrix.
Algorithm~\ref{algo:adjmat} summarizes this process.

\begin{algorithm}
\SetAlgoLined
    $N$ = Number of nodes in hypergraph $G$\;
    $\mathbf{A}$ = $N \times N$ zero matrix\;
    \ForEach{hyperedge $E$}{
        $\mathbb{E}(cut)$ = Expected value of cut of $E$\;
        \ForEach{combination of nodes $(i,j)$ in $E$}{
            $\mathbf{A}_{ij} \mathrel{+}= \frac{1}{\mathbb{E}(cut)}$\;
            $\mathbf{A}_{ji} \mathrel{+}= \frac{1}{\mathbb{E}(cut)}$\;
        }
    }
    \caption{Computing hypergraph adjacency matrix}
    \label{algo:adjmat}
\end{algorithm}

Next, we find the Laplacian matrix and solve for two of its eigenvectors
using Spectra. Spectra provides a number of tuning parameters which
can be adjusted to help the solver converge on a solution.
Some of these are the maximum number of iterations (\texttt{max\_iter}),
the rate of convergence (\texttt{ncv}),
the number of eigenpairs to find (\texttt{nev}),
which eigenpairs to find: largest or smallest (\texttt{SelectionRule}),
and the error tolerance/precision required for the eigenpairs (\texttt{tol}).
We use Spectra with the following parameters:

\begin{itemize}
    \item \texttt{max\_iter} = $10\mathrm{e}{6}$
    \item \texttt{ncv} = 6
    \item \texttt{nev} = 2
    \item \texttt{SelectionRule} = smallest
    \item \texttt{tol} = $1\mathrm{e}{-6}$
\end{itemize}

Let $v$ be the eigenvector of interest; that is, the eigenvector corresponding to the second-smallest eigenvalue.
The nodes of the hypergraph are sorted by their corresponding values
in $v$. 
An example for a set of 3 nodes is shown below.
\begin{align*}
\{A, B, C\} = \text{set of nodes in a hypergraph}\\
\overrightarrow{\mathbf{v}} =\begin{bmatrix} 5 \\ 2 \\ 3 \end{bmatrix} = \text{eigenvector of second-smallest eigenvalue}\\
\{A, B, C\} \xrightarrow{\text{sort by}~\overrightarrow{\mathbf{v}}} \{B, C, A\}
\end{align*}
The value corresponding to node $B$ in the eigenvector is the smallest, so $B$ is
sorted into the first position. Node $C$'s value is the second smallest, so it follows
$B$, and finally $A$ has the largest value and so is last.
In this ordering, the nodes are
clustered according to which of the two partitions they are best
suited to be placed in. We can then iterate over all possible
balanced partitions to find the one with the lowest hyperedge cut.
This is computationally feasible because we are only finding a 2-way partition.
Our algorithm for finding the optimal partition is shown in
Algorithm~\ref{algo:findpart}.

\begin{algorithm}
\SetAlgoLined
    $minCut$ = \texttt{MAX\_UINT}\;
    $minI$ = -1\;
    $H$ = Hypergraph to be partitioned\;
    $sortedNodes$ = Nodes of $H$ sorted using eigenvector\;
    $N_{45\%}$ = 45th percentile of $sortedNodes$\;
    $N_{55\%}$ = 55th percentile of $sortedNodes$\;
    $N_n$ = last node in $sortedNodes$\;
    \ForEach{Node $N_i \in \{N_{45\%}$ \ldots $N_{55\%}\}$}{
        $P = \text{candidate partitions} = \big\{\{N_0 \ldots N_i\}, \{N_{i+1} \ldots N_n\}\big\}$\;
        $cut = 0$\;
        \ForEach{Hyperedge $E \in H$}{
            \ForEach{Node $N_j \in E$}{
                $partitionsTouched$ = \{\}\;
                \eIf{$N_j \in P[0]$}{
                    $partitionsTouched \mathrel{+}= \{P[0]\}$\;
                }{
                    $partitionsTouched \mathrel{+}= \{P[1]\}$\;
                }
            }
            $cut \mathrel{+}= \lvert partitionsTouched \rvert - 1$\;
        }
        \If{$cut < minCut$}{
            $minCut = cut$\;
            $minI = i$\;
        }
    }
    \Return{$\{\{N_0 \ldots N_{minI}\}, \{N_{minI+1} \ldots N_n\}\}$}\;
    \caption{Finding the optimal partition using the eigenvector.}
    \label{algo:findpart}
\end{algorithm}

Once the optimal partition is found, it is written to a file
and the program will exit. Our output file format conforms
to the format specified by hMETIS~\cite{karypis_multilevel_1999}.
Each line corresponds to a node in the hypergraph, in the order 
they are numbered in the original input, and contains
the number of the partition it has been assigned to --- in
the case of two partitions, either 0 or 1.
