from sklearn.cluster import SpectralClustering
import numpy as np
import sys


hgr = sys.argv[1]
with open(hgr, "r") as hgrf:
    n_hedges, n_nodes, _ = hgrf.readline().split()
    n_nodes = int(n_nodes)
    x = np.matrix([[0]*n_nodes]*n_nodes)
    for line in hgrf:
        w, n1, n2, = line.split()
        w = int(w)
        n1 = int(n1)-1
        n2 = int(n2)-1
        x[n1,n2] += w
        x[n2,n1] += w

clustering = SpectralClustering(n_clusters=2,
                                assign_labels="discretize",
                                random_state=0).fit(x)

print clustering.labels_
