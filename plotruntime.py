import pandas
import sys
from matplotlib import pyplot as plt
import re
import numpy as np
import os

filename_re = re.compile(r'runtime-(\d+)v-(\d+)e-(\d+)o.csv')

inputs = sys.argv[1:]
indices = np.arange(len(inputs))

parseMedians = []
eigenMedians = []
clusterMedians = []
parseErrs = [[],[]]
eigenErrs = [[],[]]
clusterErrs = [[],[]]
# parseErrs = []
# eigenErrs = []
# clusterErrs = []
ticks = []

width = 0.25

for idx, inp in enumerate(inputs):
    filename_match = filename_re.match(os.path.basename(inp))
    n_nodes = int(filename_match.groups()[0])
    n_hedges = int(filename_match.groups()[1])
    max_rank = int(filename_match.groups()[2])

    data = pandas.read_csv(inp)
    parseT = data["Parse (msec)"]
    eigenT = data["Eigen Solver (msec)"]
    clusterT = data["Clustering (msec)"]
    parseMedians.append(np.median(parseT))
    eigenMedians.append(np.median(eigenT))
    clusterMedians.append(np.median(clusterT))
    # parseErrs.append(np.std(parseT))
    # eigenErrs.append(np.std(eigenT))
    # clusterErrs.append(np.std(clusterT))
    parseErrs[0].append(np.median(parseT) - np.quantile(parseT, 0.25))
    parseErrs[1].append(np.quantile(parseT, 0.75) - np.median(parseT))
    eigenErrs[0].append(np.median(eigenT) - np.quantile(eigenT, 0.25))
    eigenErrs[1].append(np.quantile(eigenT, 0.75) - np.median(eigenT))
    clusterErrs[0].append(np.median(clusterT) - np.quantile(clusterT, 0.25))
    clusterErrs[1].append(np.quantile(clusterT, 0.75) - np.median(clusterT))
    ticks.append("{} nodes\n{} h-edges\nmax rank {}".format(n_nodes, n_hedges, max_rank))


p1 = plt.barh(np.add(indices,width), parseMedians, width, xerr=parseErrs)
p2 = plt.barh(indices, eigenMedians, width, xerr=eigenErrs, left=parseMedians)
p3 = plt.barh(np.subtract(indices,width), clusterMedians, width, xerr=clusterErrs, left=np.add(parseMedians, eigenMedians))
plt.yticks(indices, ticks)
plt.legend((p1[0], p2[0], p3[0]), ('Parse', 'Find eigenpairs', 'Cluster'))

plt.ylabel("Hypergraph Size")
plt.xlabel("Runtime (milliseconds)")
plt.title("Runtime for 100 random hypergraphs of various sizes")

plt.tight_layout()
plt.show()
