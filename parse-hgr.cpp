#include <Eigen/Core>
#include <iostream>
#include <fstream>
#include <vector>
#include <tuple>
#include <set>

using namespace std;

static unsigned int choose( unsigned int n, unsigned int k )
{
    if (k > n) return 0;
    if (k * 2 > n) k = n-k;
    if (k == 0) return 1;

    int result = n;
    for( int i = 2; i <= k; ++i ) {
        result *= (n-i+1);
        result /= i;
    }
    return result;
}

tuple<Eigen::MatrixXd, set<set<int>>> parseHGR(char *hgr_file_path)
{
    int n_hedges, n_nodes;
    ifstream hgr_file(hgr_file_path);
    Eigen::MatrixXd adj;
    set<set<int>> hedges_seen;
    if(hgr_file.is_open()) {
        hgr_file >> n_hedges >> n_nodes;
        adj = Eigen::MatrixXd::Zero(n_nodes, n_nodes);
        
        string line;
        while(getline(hgr_file, line)) {
            int node;
            set<int> hedge;
            stringstream line_ss(line);
            while(line_ss >> node) {
                hedge.insert(node);
            }
            if(hedges_seen.count(hedge))
                continue;
            float expcut = 0.0;
            int N = hedge.size();
            for(int i=1; i<=(N-1); i++) {
                float prob = ((float)choose(N, i)) / ((float)(pow(2, N) - 2));
                expcut += i * (N - i) * prob;
            }
            float weight = 0.0;
            if(expcut > 0.0) {
                weight = (1.0 / expcut);
            }
            // cout << line << " costs " << weight << endl;
            for(auto n1 = hedge.begin(), ee = hedge.end(); n1 != ee; ++n1) {
                for(auto n2 = next(n1), ee = hedge.end(); n2 != ee; ++n2) {
#ifdef SIMPLEWEIGHTS
                    adj(*n1-1, *n2-1) += 1;
                    adj(*n2-1, *n1-1) += 1;
#else
                    adj(*n1-1, *n2-1) += weight;
                    adj(*n2-1, *n1-1) += weight;
#endif
                }
            }
            hedges_seen.insert(hedge);
        }
    }
    return make_tuple(adj, hedges_seen);
}


