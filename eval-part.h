#include <Eigen/Core>
#include <set>

/**
 * @brief Approximate the hyperedge cut for the partition.
 * Note that because the adjacency matrix corresponds to the
 * 2-section graph approximation of the hypergraph,
 * the returned value is not the exact hyperedge cut.
 * In reality, we are calculating the edge cut of the weighted
 * 2-section graph
 * 
 * @param adj_mat Adjacency matrix for 2-section graph
 * @param partitions Array of partition IDs, one ID per (hyper)graph node
 * @return float Hyperedge cut approximation
 */
unsigned int getPartitionHedgeCut(std::set<std::set<int>> hedges, int *partitions);

