import pandas
import sys
from matplotlib import pyplot as plt
import re
import numpy as np
import os

filename_re = re.compile(r'weights-(\d+)v-(\d+)e-(\d+)o.csv')

inputs = sys.argv[1:]
indices = np.arange(len(inputs))

hedgeCutMeans = {
    "Probabilistic": [],
    "Simple": []
}
extDegMeans = {
    "Probabilistic": [],
    "Simple": []
}
costMeans = {
    "Probabilistic": [],
    "Simple": []
}
absorptionMeans = {
    "Probabilistic": [],
    "Simple": []
}
hedgeCutStds = {
    "Probabilistic": [],
    "Simple": []
}
extDegStds = {
    "Probabilistic": [],
    "Simple": []
}
costStds = {
    "Probabilistic": [],
    "Simple": []
}
absorptionStds = {
    "Probabilistic": [],
    "Simple": []
}
ticks = []

width = 0.25

for idx, inp in enumerate(inputs):
    filename_match = filename_re.match(os.path.basename(inp))
    n_nodes = int(filename_match.groups()[0])
    n_hedges = int(filename_match.groups()[1])
    max_rank = int(filename_match.groups()[2])

    data = pandas.read_csv(inp)

    ticks.append("{} nodes\n{} h-edges\nmax rank {}".format(n_nodes, n_hedges, max_rank))
    for weight_scheme in ["Probabilistic", "Simple"]:
        data4scheme = data[data["Weight Scheme"] == weight_scheme]
        hedgeCuts = data4scheme["Hyperedge Cut"]
        extDegs = data4scheme["External Degrees"]
        scaledCost = data4scheme["Scaled Cost"]
        absorption = data4scheme["Absorption"]
        hedgeCutMeans[weight_scheme].append(np.mean(hedgeCuts))
        extDegMeans[weight_scheme].append(np.mean(extDegs))
        costMeans[weight_scheme].append(np.mean(scaledCost))
        absorptionMeans[weight_scheme].append(np.mean(absorption))
        hedgeCutStds[weight_scheme].append(np.std(hedgeCuts))
        extDegStds[weight_scheme].append(np.std(extDegs))
        costStds[weight_scheme].append(np.std(scaledCost))
        absorptionStds[weight_scheme].append(np.std(absorption))

def do_plot(axis, means, stds, title):
    axis.barh(np.add(indices,width/2), means["Probabilistic"], height=width, xerr=stds["Probabilistic"], label="Probabilistic")
    axis.barh(np.subtract(indices,width/2), means["Simple"], height=width, xerr=stds["Simple"], label="Simple")
    axis.set_xlabel(title)
    axis.set_ylabel("Hypergraph Size")
    axis.set_yticks(indices)
    axis.set_yticklabels(ticks)

fig, axes = plt.subplots(2,2)

do_plot(axes[0,0], hedgeCutMeans, hedgeCutStds, "Hyperedge Cut (Minimize)")
do_plot(axes[1,0], extDegMeans, extDegStds, "External Degrees (Minimize)")
do_plot(axes[0,1], costMeans, costStds, "Scaled Cost (Minimize)")
do_plot(axes[1,1], absorptionMeans, absorptionStds, "Absorption (Maximize)")

fig.suptitle("Partition Quality for Different Edge Weight Schemes")

plt.legend()

plt.tight_layout()
plt.show()
