import sys
import random

def gen_hgr(outputf, num_verts, num_edges, max_degree):
    outputf.write("%d %d\n" % (num_edges, num_verts))
    vertices = [i for i in range(1, num_verts+1)]

    for edge in range(0, num_edges):
        edge_verts = set([random.choice(vertices) for i in range(0, random.randint(1, max_degree+1))])
        for v in edge_verts:
            outputf.write("%d " % v)
        outputf.write("\n")


if __name__ == "__main__":
    num_verts = int(sys.argv[1])
    max_degree = int(sys.argv[3])
    num_edges = int(sys.argv[2])
    gen_hgr(sys.stdout, num_verts, num_edges, max_degree)
