Eigen3_DIR ?= /usr/include/eigen3

CXXFLAGS=
CXXFLAGS+=-I$(Eigen3_DIR)
CXXFLAGS+=-Ispectra-0.8.1/include
CXXFLAGS+=-I.

#CXXFLAGS+=-DEIGEN_DONT_VECTORIZE
#CXXFLAGS+=-DEIGEN_DISABLE_UNALIGNED_ARRAY_ASSERT

all: hgr-part hgr-part-simpleweights

SRC=hgr-part.cpp parse-hgr.cpp asa136.cpp eval-part.cpp
RECIPE=/usr/bin/g++ -std=c++11 ${CXXFLAGS} -g -O0 $^ -o $@

hgr-part: $(SRC)
	$(RECIPE)	

hgr-part-simpleweights: CXXFLAGS+=-DSIMPLEWEIGHTS
	

hgr-part-simpleweights: $(SRC)
	$(RECIPE)

clean:
	rm -f hgr-part hgr-part-simpleweights
